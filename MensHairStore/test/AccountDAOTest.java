import DAL.AccountDAO;
import static org.junit.Assert.*;

import java.util.ArrayList;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import model.Account;

public class AccountDAOTest {

    private AccountDAO accountDAO;

    @Before
    public void setUp() {
        // Thiết lập các phụ thuộc hoặc cấu hình cần thiết
        accountDAO = new AccountDAO();
    }

    @After
    public void tearDown() {
        // Dọn dẹp sau mỗi phương thức kiểm thử nếu cần
    }

    @Test
    public void testInsertAccount() {
        // Tạo một tài khoản mới để chèn vào cơ sở dữ liệu
        Account account = new Account(1, "testuser", "testpassword", "test@gmail.com", 1);

        // Chèn tài khoản vào cơ sở dữ liệu
        accountDAO.insertAccount(account);

        // Kiểm tra xem tài khoản đã được chèn thành công hay không
        // Bạn có thể sử dụng các phương thức khác của AccountDAO để kiểm tra
        // ví dụ: getAccountByUsername, getAccountById

        Account insertedAccount = accountDAO.getAccount("testuser","testpassword");
        
        assertNotNull(insertedAccount);
        assertEquals(account.getUsername(), insertedAccount.getUsername());
        assertEquals(account.getPassword(), insertedAccount.getPassword());
        assertEquals(account.getGmail(), insertedAccount.getGmail());
        assertEquals(account.getRole_id(), insertedAccount.getRole_id());
    }

    @Test
    public void testGetAccount() {
        // Tạo tài khoản mới trong cơ sở dữ liệu để kiểm tra
        Account account = new Account(2, "testuser2", "testpassword2", "test2@gmail.com", 2);
        accountDAO.insertAccount(account);

        // Lấy tài khoản từ cơ sở dữ liệu bằng username và password
        Account retrievedAccount = accountDAO.getAccount(account.getUsername(), account.getPassword());

        // Kiểm tra xem tài khoản đã được lấy thành công hay không
        
        assertNotNull(retrievedAccount);
        assertEquals(account.getUsername(), retrievedAccount.getUsername());
        assertEquals(account.getPassword(), retrievedAccount.getPassword());
        assertEquals(account.getGmail(), retrievedAccount.getGmail());
        assertEquals(account.getRole_id(), retrievedAccount.getRole_id());
        
    }

    @Test
    public void testGetAllAccount() {
        // Lấy danh sách tất cả các tài khoản từ cơ sở dữ liệu
        ArrayList<Account> accountList = accountDAO.getAllAccount();

        // Kiểm tra xem danh sách tài khoản có rỗng hay không
        
        assertFalse(accountList.isEmpty());
    }

    @Test
    public void testDeleteAccount() {
        // Tạo một tài khoản mới để chèn vào cơ sở dữ liệu
        Account account = new Account(3, "testuser3", "testpassword3", "test3@gmail.com", 3);
        accountDAO.insertAccount(account);

        // Xóa tài khoản từ cơ sở dữ liệu
        accountDAO.deleteAccount("3");
        

        // Kiểm tra xem tài khoản đã bị xóa thành công hay không
        Account deletedAccount = accountDAO.getAccountById("3");
        assertNull(deletedAccount);
    }
}